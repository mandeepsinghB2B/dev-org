trigger CasePostTrigger on FeedItem (after insert) {

set<id> ids = new set<id> ();
      for (FeedItem item : Trigger.new) {
        // Only work on posts on Cases, anything else, we ignore
        ids.add(item.id);            
      }
String communityId = null;

//Set<String> feedElementId = (Set<String>)JSON.deserialize(JSON.serialize(ids), Set<String>.class);

String feedElementId = (String)JSON.deserialize(JSON.serialize(ids),String.class);

ConnectApi.CommentInput commentInput = new ConnectApi.CommentInput();
ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

textSegmentInput.text = 'Does anyone in this group have an idea? ';
messageBodyInput.messageSegments.add(textSegmentInput);

mentionSegmentInput.id = '00512000005smgN';
messageBodyInput.messageSegments.add(mentionSegmentInput);

commentInput.body = messageBodyInput;

// ConnectApi.Comment commentRep = ConnectApi.ChatterFeeds.postCommentToFeedElement(communityId, feedElementId, commentInput, null);

ConnectApi.ChatterFeeds.postComment(null, feedElementId, commentInput, null);

}