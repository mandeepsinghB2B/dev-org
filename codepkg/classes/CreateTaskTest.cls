@isTest
private class CreateTaskTest{
static testMethod void CreateTaskValidate(){

        // Obtain Apex governor limits and resources for this test
        Test.startTest();
        
        Account a = new Account(Name = 'Acme');
        insert a; 
    
        Contact c = new Contact(LastName = 'Doe', AccountId = a.Id);
        insert c;  
    
        Opportunity o = new Opportunity(Name = 'test opp', 
                                        AccountId = a.Id, 
                                        CloseDate = date.today(), 
                                        StageName = 'Prospecting', 
                                        Probability = 20.0,
                                        Amount = 10);
        insert o;
        
        if (o.StageName == 'Prospecting')
        {
            o.StageName = 'Test B';
        }
        else 
        {
            o.StageName = 'Test';
        }
           
        update o;

        Test.stopTest();       
        }   
}