public class LeadConvert{ 
@future 
public static void FutureFunction(String LeadId,String AccountId,String OwnerId){ 
Database.LeadConvert lc = new Database.LeadConvert(); 
lc.setLeadId(LeadId); 
lc.setOwnerId(OwnerId); 
lc.setAccountId(AccountId); 
lc.setConvertedStatus('Qualified'); 
list<Database.LeadConvert> LeadConvertList = new list<Database.LeadConvert>(); 
LeadConvertList.add(lc); 
if (!LeadConvertList.isEmpty()) { 
// if one fails, allow others to succeed 
list<Database.LeadConvertResult> lcrList = Database.convertLead(LeadConvertList, false); 
} 
} 
}