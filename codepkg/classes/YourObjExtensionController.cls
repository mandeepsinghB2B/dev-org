public with sharing class YourObjExtensionController {

    public PTO_Request__c MyObject { get; set; }

    public YourObjExtensionController(ApexPages.StandardController controller) {
        MyObject = (PTO_Request__c)controller.getRecord();
    }

    public PageReference save() {

        // do something with MyObject.Lookup_Field__c
        // save impl here (insert/upsert)
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There is Product !!!!  ' + MyObject.Id));
        return null;
    }
}