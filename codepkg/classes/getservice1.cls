@RestResource(urlMapping='/GetService/*')  
global with sharing class getservice1
{      
     
     @HttpGet     
     global static String postRestMethod()     
     {          
          return 'Hi, You have invoked getservice1 created using Apex Rest and exposed using REST API';        
     }
}