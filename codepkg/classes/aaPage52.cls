public class aaPage52 {

    public aaPage52(ApexPages.StandardController controller) {

    }

    // Our test variable        
    public String myString {get; set;}

    public aaPage52 (){
        myString = '';
    }

    // Method for testing a hidden field functionality 
    public PageReference myMethod(){
        System.debug('myString: ' + myString);
        return null;
    }
}